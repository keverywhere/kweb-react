import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class HelperTest {

    private lateinit var helper: Helper

    @BeforeTest
    fun setUp() {
        helper = Helper()
    }

    @Test
    fun thingsShouldWork() {
        assertEquals("Hello, Den!", helper.buildWelcomeMessage("Den"))
        assertEquals("Hello, Maxim!", helper.buildWelcomeMessage("Maxim"))
    }
}