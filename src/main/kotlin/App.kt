import kotlinx.html.classes
import kotlinx.browser.window
import react.RBuilder
import react.dom.h1
import react.dom.render

val document = window.document
fun messageGenerator(name: String) = Helper().buildWelcomeMessage(name)

fun RBuilder.hello(name: String) {
    h1 {
        attrs {
            classes = setOf("title")
        }
        +messageGenerator("Max")
    }
}

fun RBuilder.app() {
    hello("Max")
}

fun main() {
    val appRoot = document.querySelector("#app") ?: return
    render(appRoot) {
        app()
    }
}